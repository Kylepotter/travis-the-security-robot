#Travis the Security robot has a list of known users. He will ask you for your name. If your name is not on the list, he will ask you if you want to be added to the list. 
#If you are on the list, he will ask you if you wanna be removed from the system.

known_users = ["Alice", "Bob", "Claire", "Dan", "Emma", "Fred", "Georgie", "Harry"]

while True:
    print("Hi! My name is Travis")
    name = input("What is your name?: ").strip().capitalize()

    if name in known_users:
        print("Hello {}!".format(name))
        remove = input("Would you like to be removed from the system (y/n):").strip().lower()

        if remove == "y":
            known_users.remove(name)
        elif remove == "n":
            print("No Problem, I didn't want you to leave anyway :)")

    else:
        print("hmmm... I don't think I've met you yet {}".format(name))
        add_me = input("Would you like to be added to the system (y/n)?: ").strip().lower()
        if add_me == "y":
            known_users.append(name)
        elif add_me == "n":
            print("Peace out.")
